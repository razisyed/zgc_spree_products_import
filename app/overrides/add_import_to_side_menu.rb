Deface::Override.new(
  virtual_path: 'spree/admin/shared/_main_menu',
  name: 'subscriptions_admin_tab',
  insert_after: 'ul#sidebarConfiguration',
  partial: 'spree/admin/shared/import_side_menu'
)
